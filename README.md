# Burrito builder

# Checkout the Live version [here](http://burritoproject.s3-website-us-east-1.amazonaws.com/)!

**Features**
*  Select or cancel ingredients
*  See visuals of the buritto in building proces

**Stack**
*  W3.css for layout
*  Bootstrap and Materialize for components and effects
*  jQuery

**Possible future steps**
*  Price system and checkout function
*  Responsive design
