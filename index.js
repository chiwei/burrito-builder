const white_rice = `<img src="./images/white_rice.png" class="ingredients rice" data-ingredient="white_rice">`

const brown_rice = `<img src="./images/brown_rice.png" class="ingredients rice" data-ingredient="brown_rice">`

const black_beans = `<img src="./images/black_beans.png" class="ingredients bean" data-ingredient="black_beans">`

const pinto_beans = `<img src="./images/pinto_beans.png" class="ingredients bean" data-ingredient="pinto_beans">`

const steak = `<img src="./images/steak.png" class="ingredients filling" data-ingredient="steak">`

const chicken = `<img src="./images/chicken.png" class="ingredients filling" data-ingredient="chicken">`

const fajita = `<img src="./images/fajita.png" class="ingredients filling" data-ingredient="fajita">`

const sofritas = `<img src="./images/sofritas.png" class="ingredients filling" data-ingredient="sofritas">`

const carnitas = `<img src="./images/carnitas.png" class="ingredients filling" data-ingredient="carnitas">`

const barbacoa = `<img src="./images/barbacoa.png" class="ingredients filling" data-ingredient="barbacoa">`

const cheese = `<img src="./images/cheese.png" class="ingredients topping" data-ingredient="cheese">`

const salsa = `<img src="./images/salsa.png" class="ingredients topping" data-ingredient="salsa">`

const sour_cream = `<img src="./images/sour_cream.png" class="ingredients topping" data-ingredient="sour_cream">`

const lettuce = `<img src="./images/lettuce.png" class="ingredients topping" data-ingredient="lettuce">`

const corn = `<img src="./images/corn.png" class="ingredients topping" data-ingredient="corn">`

const guacamole = `<img src="./images/guacamole.png" class="ingredients topping" data-ingredient="guacamole">`

const chip = `
<div class="chip">
  <span></span><i class="close material-icons">close</i>
</div>
`

$(document).ready(function(){
  $('#options input').change(function(){
    if (this.checked){
      $('#burrito').append(eval(this.id))
      $('#bag').append(chip).find('.chip').last().find('span').text($(this).next().text()).next().attr('data-ingredient',this.id).click(function(){
        var ingredient = $(this).attr('data-ingredient')
        $('#'+ingredient).prop('checked',false)
        $('#burrito img[data-ingredient='+ingredient+']').remove()
      })
    }else{
      $('#burrito img[data-ingredient='+this.id+']').remove()
      $('#bag i[data-ingredient='+this.id+']').parent().remove()
    }
  })
})
